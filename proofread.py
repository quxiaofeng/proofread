#!/usr/bin/env python
''' A simple proofread program for scientific papers
this script deals with rule based suggestions for remove/replace
by LongGang Pang from UC Berkeley'''
import os
import re
import json
import argparse

parser = argparse.ArgumentParser(description='Proofread scientific papers to make it brief and simple')
parser.add_argument('filenames', metavar='f', type=str, nargs='+',
                    help='one file or a list of files for analysis')
parser.add_argument('--brief', action='store_true', help='turn off output for the original sentence')
args = parser.parse_args()

#### add some colors to the output
color_black = lambda text: '\033[0;30m' + text + '\033[0m'
color_red = lambda text: '\033[0;31m' + text + '\033[0m'
color_green = lambda text: '\033[0;32m' + text + '\033[0m'
color_yellow = lambda text: '\033[0;33m' + text + '\033[0m'
color_blue = lambda text: '\033[0;34m' + text + '\033[0m'
color_magenta = lambda text: '\033[0;35m' + text + '\033[0m'
color_cyan = lambda text: '\033[0;36m' + text + '\033[0m'
color_white = lambda text: '\033[0;37m' + text + '\033[0m'

# read rules from json file
with open("rules.json", "r") as json_data:
    rules = json.load(json_data)

# create new rules by replacing 'is' to 'was', 'has been', ...
def augment(sentence):
    """change 'is' in the sentence to was, has been, 
    should be ... to augment some new sentences
    Args:
      sentence: str, candidate sentence to be removed/replaced
    Return:
      augmented_strs: array of new candidates
    """
    pad_sentence = " " + sentence + " "
    augmented_str = [pad_sentence]
    if 'is' in pad_sentence:
        index = pad_sentence.find("is")
        reps = ["was", "have been", "has been", "had been", "should be"]
        for replace_candidate in reps:
            new_str = pad_sentence[:index]
            new_str += replace_candidate
            new_str += pad_sentence[index+2:]
        augmented_str.append(new_str)
    return augmented_str


def get_context(src_text, index):
    '''get the full sentence that contain the position index'''
    stop_puncs = ['.', ',', '!', '?', ';', ':', '\n']
    istart = max([src_text[:index].rfind(st) for st in stop_puncs])
    iend = min([src_text[index:].find(st) for st in stop_puncs if src_text[index:].find(st)!=-1])
    return istart, iend
        
# create suggestions for sentences to remove
def suggest_remove(file_path, to_remove, verbose=True):
    with open(file_path, "r") as fsrc:
        src_text = fsrc.read().lower()
    suggestions = []
    for item in to_remove:
        for s in augment(item):
            if s not in src_text: continue
            suggestions.append(color_red("remove: "+s))
            if verbose:
                indices = [m.start() for m in re.finditer(s, src_text)]
                for index in indices:
                    istart, iend = get_context(src_text, index)
                    ctx = src_text[istart+1:index]+color_red(src_text[index:index+len(s)])
                    ctx += src_text[index+len(s):index+iend]
                    suggestions.append(ctx)
    return suggestions


# create suggestions for sentences to replace
def suggest_replace(file_path, to_replace, verbose=True):
    with open(file_path, "r") as fsrc:
        src_text = fsrc.read().lower()
    suggestions = []
    for key, value in to_replace.items():
        for s in augment(key):
            if s not in src_text: continue
            suggestions.append(color_red("replace: "+s)+" ---> "+color_magenta(value))
            if verbose:
                indices = [m.start() for m in re.finditer(s, src_text)]
                for index in indices:
                    istart, iend = get_context(src_text, index)
                    ctx = src_text[istart+1:index]+color_red(src_text[index:index+len(s)])
                    ctx += src_text[index+len(s):index+iend]
                    suggestions.append(ctx)
    return suggestions

if __name__ == '__main__':
    to_remove = rules["to_remove"]
    to_replace = rules["to_replace"]
    verbose = not args.brief
    for fname in args.filenames:
        sug1 = suggest_remove(fname, to_remove, verbose)
        sug2 = suggest_replace(fname, to_replace, verbose)
        suggestions = sug1 + sug2
        # if no suggestions, continue to process next file
        if len(suggestions) == 0: continue
        # otherwise print suggestions to screen
        print(color_blue("+++++++++*********************************************"))
        print("Suggestions for ", color_cyan(fname.split('/')[-1]))
        for suggestion in suggestions:
            print(suggestion)
            print("")
