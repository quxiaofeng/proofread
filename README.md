# proofread

庞龙刚，UC Berkeley 物理系
lgpang@qq.com

这是一个个人开源项目，目的是对科学文章草稿自动检查，寻找不符合英文科学写作标准的部分，并提出修改意见。
这个程序目前并不检查单词拼写错误。它只负责提出建议将多余的词，不准确的词删除或替换。

## 使用方法
在命令行输入如下命令，下载并运行
```bash
git clone https://gitlab.com/snowhitiger/proofread.git
cd proofread
python proofread.py path/*.tex
```
其中星号代表任何文件名, 或星号匹配的所有文件，运行proofread.py输出结果如下:
![example](imgs/example_output.png)


## 基于规则的替换建议
在英文科学写作中，会有很多明确的规则，以使文章更清晰，明了，面向大众。
这个项目的最初版本会基于规则，对出现的特定词组建议替换或删除。
举例说明几个简单的规则：

1. "As it is well known"：建议删除。
2. "It is shown that": 建议删除。
3. "is related to": 建议修改成 "is associated with"。
4. "provides a review": 建议修改成 "reviews"。

这些规则会有很多变体，比如 "As it is well known" 在文章中可能表现为 "As it has been well know" 或者 
"As it was well known"。基于规则的这部分会尽量考虑所有的可能并提出相应的替换方案。

## 基于自然语言处理的语法判断和语义分析
ToDo

有些需要语义判断的地方，可能会用到自然语言处理。比如 Since 一般被用作与时间相关的语句，但有时候它也会被
用作 “because”，如果我们想将所有与时间无关的用例都替换成 "because" 或者 "as", 那么就需要使用一定
的机器学习或人工智能，来判断语句是否表达了自某一时刻起。

自然语言处理的方法，可以是简单的隐马尔可夫链/条件随机场，可以是复杂点的RNN，也可以是更复杂的BERT。

训练语料可以通过将文章中的 because 替换成 since，作为训练样例。


## 规则资源

* [Scientific Writing and Communication: Papers, Proposals, and Presentations 3rd Edition](https://www.amazon.com/Scientific-Writing-Communication-Proposals-Presentations/dp/0190278544/ref=sr_1_1?crid=LL6BK57HDFO4&keywords=scientific+writing+and+communication&qid=1555175104&s=gateway&sprefix=scientific+writting+%2Caps%2C183&sr=8-1)
* [Duke: Scientific Writting Resource](https://cgi.duke.edu/web/sciwriting/index.php?action=lesson3)
